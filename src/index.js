// @flow
import React from 'react';
import ReactDOM from 'react-dom';
import { HashRouter as Router, Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store from 'src/store';
import { setVehiclesList } from 'src/actions';

import fetchVehicles from 'src/api/vehicles';
import VehiclesMap from 'src/screens/VehiclesMap';
import VehiclesTable from 'src/screens/VehiclesTable';
import 'antd/dist/antd.css';


function loadData() {
  fetchVehicles()
    .then(({ data }) => setVehiclesList(data))
    .finally(setTimeout(loadData, 30000));
}

loadData();

const application = (
  <Provider store={store}>
    <Router>
      <Route
        exact
        path={['/map', '/map/focus/:id']}
        render={({ match }) => (
          <VehiclesMap focusVehicleID={Number(match.params.id)} />
        )}
      />
      <Route exact path={['/table', '/']} component={VehiclesTable} />
    </Router>
  </Provider>
);

const root = document.getElementById('root');

if (root) ReactDOM.render(application, root);
