// @flow
import React from 'react';
import { useSelector } from 'react-redux';
import isEqual from 'lodash/isEqual';
import { Table } from 'antd';
import { getVehiclesList } from 'src/selectors';
import Router from 'modules/locationRouter';


export default function VehiclesTable() {
  const columns = [
    {
      title: 'ID',
      dataIndex: 'id',
      key: 'id',
    },
    {
      title: 'License Plate',
      dataIndex: 'licensePlate',
      key: 'licensePlate',
    },
    {
      title: 'Location',
      dataIndex: 'address',
      key: 'address',
    },
  ];
  const dataSource = useSelector(getVehiclesList, isEqual).map(item => ({ ...item, key: item.id }));

  function onRow({ id }) {
    return { onClick: () => Router.go(`map/focus/${id}`) };
  }

  return <Table dataSource={dataSource} columns={columns} pagination={{ defaultPageSize: 15 }} onRow={onRow} />;
}
