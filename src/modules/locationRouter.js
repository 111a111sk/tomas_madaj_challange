class LocationRouter {
  static silentGo = (location) => {
    window.location.replace(`#${location}`);
  }

  static go = (location) => {
    window.location.hash = location;
  }

  static back = () => {
    window.history.back();
  }

  static currentPage = () => {
    return window.location.hash;
  };
}

export default LocationRouter;
