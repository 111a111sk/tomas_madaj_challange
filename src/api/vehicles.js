// @flow
import connection from './connection';
import apiConfig from './config';


const fetchVehicles = () => connection.get('vehicles', { params: { branchId: apiConfig.BRANCH_ID } });


export default fetchVehicles;
