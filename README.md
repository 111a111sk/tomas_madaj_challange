## Running dev-server ##
For development in browser with support of Webpack's HMR you should simply `npm i` and `npm run dev`. After that you can access the project on your local address (also visible in the output of your console).
