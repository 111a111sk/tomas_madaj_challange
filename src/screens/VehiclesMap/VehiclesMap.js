// @flow
import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import isEqual from 'lodash/isEqual';
import Router from 'modules/locationRouter';
import { getVehiclesList } from 'src/selectors';
import { Map, Marker, GoogleApiWrapper } from 'google-maps-react';
import { Button } from 'antd';


type Props = {
  focusVehicleID: ?number,
  google: any,
}

function VehiclesMap({ google, focusVehicleID }: Props) {
  const vehicles = useSelector(getVehiclesList, isEqual);
  const [center, setMapCenter] = useState();

  function onMarkerClick(marker) {
    setMapCenter(marker.position);
  }

  function findFocusedVehicle() {
    const { position: { coordinates } } = vehicles.find(({ id }) => id === focusVehicleID);

    return {
      lat: coordinates[1],
      lng: coordinates[0],
    };
  }

  function getCenter() {
    const vienna = {
      lat: 48.208501,
      lng: 16.373135,
    };

    return focusVehicleID && vehicles.length ? findFocusedVehicle() : vienna;
  }

  function goToTable() {
    Router.go('table');
  }

  function renderMarkers() {
    const goldStar = {
      path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
      fillColor: 'yellow',
      fillOpacity: 1,
      scale: 0.15,
      anchor: new google.maps.Point(123, 115),
      strokeColor: 'gold',
      strokeWeight: 3,
    };

    return vehicles.map(({ id, position: { coordinates } }) => (
      <Marker
        key={id}
        position={{ lat: coordinates[1], lng: coordinates[0] }}
        onClick={onMarkerClick}
        icon={id === focusVehicleID ? goldStar : null}
      />
    ));
  }

  return (
    <>
      <Map google={google} zoom={12} initialCenter={getCenter()} center={center}>
        {renderMarkers()}
      </Map>
      <Button
        size="large"
        type="primary"
        onClick={goToTable}
        style={{ position: 'absolute', bottom: 0, margin: '3%' }}
      >
        Table
      </Button>
    </>
  );
}


export default GoogleApiWrapper({ apiKey: 'AIzaSyDZDKKkxWcmDpb3-kLGh50W_UPZUFIQDoE' })(VehiclesMap);
