import { createStore, compose, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { routerReducer, routerMiddleware } from 'react-router-redux';
import { createBrowserHistory } from 'history';
import reducers from './reducers';

const history = createBrowserHistory();
const middlewareForRouter = routerMiddleware(history);
/* eslint-disable no-underscore-dangle */
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

const store = createStore(
  combineReducers({ ...reducers, routerReducer }),
  composeEnhancer(applyMiddleware(thunk, middlewareForRouter)),
);

const observeStore = (selector, onChange) => {
  let currentState;

  function handleChange() {
    const nextState = selector();

    if (nextState !== currentState) {
      currentState = nextState;
      onChange(currentState);
    }
  }

  const unsubscribe = store.subscribe(handleChange);

  handleChange();

  return unsubscribe;
};

export { store as default, history, observeStore };
