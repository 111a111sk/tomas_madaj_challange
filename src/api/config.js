const apiConfig = {
  URL_BASE: 'https://core.staging.gourban.eu/front',
  CONTENT_TYPE: 'application/x-www-form-urlencoded',
  BRANCH_ID: 3,
};

export default apiConfig;
