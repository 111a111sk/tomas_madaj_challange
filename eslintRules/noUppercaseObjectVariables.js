const UPPERCASE_AND_UNDERSCORES_REGEXP = /^[A-Z_]*$/;

/**
 ** The rule that disallows "CONSTANT_LIKE" naming for Objects:
 *
 ** This will fail:
 * const SOME_NAME = {
 *   foo: 42;
 * };
 *
 * @param context
 */
module.exports = context => ({
  VariableDeclarator: (node) => {
    const variableName = node.id && node.id.name;
    const initializerType = node.init && node.init.type;

    if (variableName && initializerType === 'ObjectExpression' && UPPERCASE_AND_UNDERSCORES_REGEXP.test(variableName)) {
      context.report(
        node,
        'Invalid variable name: {{name}}. UPPER_CASE names should not be used for object literal variables',
        { name: variableName },
      );
    }
  },
});
