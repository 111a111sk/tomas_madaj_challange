const webpack = require('webpack');
const path = require('path');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const { getIfUtils, removeEmpty } = require('webpack-config-utils');
const getIPAddress = require('./webpackConfig/utils.js');

module.exports = (env) => {
  const { ifProd, ifDev } = getIfUtils(env);
  const port = 10007;
  const HotMiddleware = ifDev([
    'react-hot-loader/patch',
  ], []);
  const hotUpdateFilenames = ifDev({
    hotUpdateChunkFilename: 'hot/[id].[hash].hot-update.js',
    hotUpdateMainFilename: 'hot/[hash].hot-update.json',
  }, {});
  const output = {
    filename: 'bundle.js',
    path: path.resolve('dist'),
    pathinfo: ifDev(),
    publicPath: ifDev(`http://${getIPAddress(env)}:${port}/dist/`, './dist/'),
    ...hotUpdateFilenames,
  };
  const devServer = ifDev({
    clientLogLevel: 'info',
    historyApiFallback: true,
    host: getIPAddress(),
    port,
    hot: true,
  }, {});

  return {
    entry: removeEmpty([
      ...HotMiddleware,
      './src/index.js',
    ]),
    output,
    devServer,
    devtool: ifDev('source-map'),
    module: {
      rules: [
        {
          use: [ // bottom to top order!
            'babel-loader',
            'eslint-loader',
          ],
          test: /\.js$/,
          exclude: [path.resolve(__dirname, 'node_modules')],
        },
        {
          test: /\.css$/,
          loader: 'style-loader!css-loader',
        },
      ],
    },
    context: __dirname,
    mode: ifProd('production', 'development'),
    resolve: {
      alias: {
        assets: path.resolve(__dirname, 'assets/'),
        modules: path.resolve(__dirname, 'src/modules/'),
        src: path.resolve(__dirname, 'src/'),
      },
    },
    plugins: removeEmpty([
      new CleanWebpackPlugin(),
      ifDev(new webpack.NamedModulesPlugin()),
      // new webpack.ProvidePlugin({ Promise: 'bluebird' }),
      ifDev(new webpack.HotModuleReplacementPlugin()),
      ifDev(new webpack.NoEmitOnErrorsPlugin()),
    ]),
  };
};
