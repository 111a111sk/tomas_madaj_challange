// @flow
import axios from 'axios';
import apiConfig from './config';


const baseConfig = {
  baseURL: apiConfig.URL_BASE,
  headers: {
    Accept: 'application/json',
    'Content-Type': apiConfig.CONTENT_TYPE,
  },
};

const connection = axios.create(baseConfig);


export default connection;
