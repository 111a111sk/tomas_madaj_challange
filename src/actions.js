// @flow
import store from 'src/store';


/* eslint-disable import/prefer-default-export */
export const setVehiclesList = (vehiclesList: Array<Object>) => {
  store.dispatch({
    type: 'SET_VEHICLES_LIST',
    vehiclesList,
  });
};
/* eslint-enable */
