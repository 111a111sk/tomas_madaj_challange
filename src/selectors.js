// @flow
import store from './store';


/* eslint-disable import/prefer-default-export */
export const getVehiclesList = (): Array<Object> => store.getState().vehiclesReducer.vehiclesList;
/* eslint-enable */
