// @flow
type Vehicle = {
  address: string,
  id: number,
  licensePlate: string,
  numberOfPersons: number,
  position: {
    type: string,
    coordinates: [number, number],
  },
  remainingKilometers: number,
  stateOfCharge: number,
  type: string,
  typeName: string,
}

type VehiclesState = {
  vehiclesList: Array<Vehicle>,
}

const defaultVehiclesState = {
  vehiclesList: [],
};

function vehiclesReducer(state: VehiclesState = defaultVehiclesState, action: Object): Object {
  switch (action.type) {
    case 'SET_VEHICLES_LIST':
      return {
        ...state,
        vehiclesList: action.vehiclesList,
      };
    default:
      return state;
  }
}

export default { vehiclesReducer };
