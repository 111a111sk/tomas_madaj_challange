const webpackConfig = require('./webpack.config.js');

module.exports = {
  "extends": [
    "plugin:flowtype/recommended",
    "airbnb"
  ],
  "env": {
    "browser": true,
    "es6": true,
  },
  "parser": "babel-eslint",
  "parserOptions": {
    "ecmaVersion": 10,
    "sourceType": "module",
    "ecmaFeatures": {
    },
  },
  "plugins": [
    "flowtype"
  ],
  "settings": {
    "import/resolver": {
      "webpack": {
          "config": {
              "resolve": webpackConfig('dev').resolve
          }
      }
    },
    "flowtype": {
        "onlyFilesWithFlowAnnotation": true,  // @flow annotation
    },
  },
  "rules": {
    "max-len": ["warn", 120],
    "max-statements": ["warn", 20],
    "arrow-body-style": ["off", "as-needed"],  // by default doesn't allow fn bodies with only return statement
    "object-curly-newline": ["error", { "ObjectPattern": { "consistent": true, "multiline": true, "minProperties": 0 } }],  // allow long param deconstructions
    "prefer-destructuring": "warn",
    "no-console": ["warn", { "allow": [ "error", "warn", "info"] }],
    "no-plusplus": "off",
    "no-duplicate-imports": "error",
    "import/no-named-as-default": "off",
    // "flowtype/delimiter-dangle": ["error", "always-multiline"],
    // "flowtype/semi": ["error", "always"]

    "react/jsx-indent": ["error", 2],
    "react/jsx-indent-props": ["error", 2],
    "react/jsx-filename-extension": ["error", { "extensions": [".js"] }],
    "react/require-extension": "off",
    "react/sort-comp": [
        "error",
        {
            "order": [
                "type-annotations",
                "static-methods",
                "lifecycle",
                "/^on.+$/",
                "/^(get|set)(?!(InitialState$|DefaultProps$|ChildContext$)).+$/",
                "everything-else",
                "/^render.+$/",
                "render"
            ]
        }
    ],
  },
};
